### Description

### Installation

1. [Installer l'extension Cadastre][1]
2. [Télécharger les données EDIGEO][2] de votre territoire. Dézipper dans un dossier.
3. [Télécharger les données FANTOIR][3] de votre territoire. Dézipper dans un dossier et supprimer éventuellement les départements non voulus.
4. [Créer une connexion PostGIS][4] en renseignant les paramètres de connexion à votre base de données. Bien vérifier que l'utilisateur dispose des droits de création et d'édition de données.

**Important** : utiliser une authentification via l'onglet `De Base` où il faudra saisir votre utilisateur / mot de passe malgré l'avertissement de QGIS, l'utilisation d'une `Configuration` ne fonctionnant pas à ce jour. Une fois les imports EDIGEO effectués, cette authentification pourra être de type `Configuration` pour une utilisation de l'extension.

5. [Créer un nouveau schéma (conseillé) et importer les données EDIGEO][5]. Pour 8000 fichiers EDIGEO, compter 4h d'import. Même si l'extension et l'application QGIS ne répondent plus, l'import s'effectue quand même. Il est possible de vérifier le déroulemement des opérations en regardant les requêtes effectuées au niveau de PostgreSQL.
6. Télécharger / Cloner le fichier `cerema2plugin.sql` de ce dépôt. Modifier le nom du schéma `qgis` par le nom du schéma où les données EDIGEO ont été chargées. Les fichiers FANTOIR doivent être présents dans le même dossier.
7. Modifier les paramètres (premières lignes) du fichier `cerema2plugin.sql`. Les paramètres sont le nom du lot et l'année des données CEREMA.
8. Adapter les lignes d'import des fichiers FANTOIR (une ligne par département). Exemple de l'import du fichier FANTOIR pour le Bas-Rhin (67) :
    ```sql
    \COPY qgis.fanr(tmp) FROM '670.txt' DELIMITER ',' ENCODING 'UTF8';
    ```
9. Exécuter le script SQL de ce dépôt :
    ```sh
    psql -U nom_utilisateur -h ip_base -p port_base -d nom_base -f cerema2plugin.sql
    ```
10. *Facultatif* : uniformiser le nom du lot pour toutes les données de l'extension Cadastre. Requête à jouer puis exécuter en tant que requêtes le résultat :
    ```sql
    SELECT 'UPDATE TABLE ' || table_schema || '.' || table_name || ' SET ' || column_name || ' = (SELECT lot FROM import);'
    FROM information_schema.columns
    WHERE
        (table_name LIKE 'geo_%' OR table_name = 'parcelle_info')
        AND column_name = 'lot'
    ;
    ```
11. *Bonus* : si vous souhaitez avoir accès à toutes les données de l'Eurométropole de Strasbourg, éditer le fichier `ogr_pci_express_67.bat` pour renseigner les paramètres de connexion à PostgreSQL. Comme au point #7, éditer cette fois-ci le fichier `ajout_eurometropole.sql`. Ouvrir une invite de commande et exécuter `ogr_pci_express_67.bat`.


[1]: https://docs.3liz.org/QgisCadastrePlugin/extension-qgis/installation/
[2]: https://docs.3liz.org/QgisCadastrePlugin/extension-qgis/donnees/
[3]: https://www.collectivites-locales.gouv.fr/competences/la-mise-disposition-gratuite-du-fichier-des-voies-et-des-lieux-dits-fantoir
[4]: https://docs.qgis.org/latest/fr/docs/user_manual/managing_data_source/opening_data.html?#creating-a-stored-connection
[5]: https://docs.3liz.org/QgisCadastrePlugin/extension-qgis/import/
