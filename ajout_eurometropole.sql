-- PARAMETRES
CREATE TEMPORARY TABLE import AS
    SELECT
        'ajout_ems'::text AS lot,
        '2022'::text AS annee
;

DELETE FROM qgis.geo_commune WHERE lot IN (SELECT lot FROM import);
DELETE FROM qgis.geo_section WHERE lot IN (SELECT lot FROM import);
DELETE FROM qgis.geo_parcelle WHERE lot IN (SELECT lot FROM import);
DELETE FROM qgis.geo_label WHERE object_rid IN ('Attribut_TEXT_id_Objet_');

INSERT INTO qgis.geo_commune (
    geo_commune, annee, object_rid, idu, tex2, creat_date, update_dat, commune, lot, ogc_fid, geom
)
SELECT
    co.code_dep || '0' || SUBSTR(co.code_insee, 3, 3) AS geo_commune,
    (SELECT annee FROM import) AS annee,
    'Object_' AS object_rid,
    SUBSTR(co.code_insee, 3, 3) AS idu,
    UPPER(co.nom_com) AS tex2,
    NOW()::date AS creat_date,
    NOW()::date AS update_dat,
    co.code_dep || '0' || SUBSTR(co.code_insee, 3, 3) AS commune,
    (SELECT lot FROM import) AS lot,
    ROW_NUMBER() OVER() + (SELECT MAX(ogc_fid) FROM qgis.geo_commune) AS ogc_fid,
    co.wkb_geometry AS geom
FROM import.commune co
WHERE
    co.code_insee IN (
        '67001', '67043', '67049', '67118', '67124', '67131',
        '67137', '67152', '67204', '67212', '67218', '67256',
        '67267', '67268', '67296', '67309', '67326', '67343',
        '67365', '67378', '67389', '67447', '67471', '67482',
        '67506', '67519', '67551'
    )
;  -- 27

INSERT INTO qgis.geo_section (
    geo_section, annee, object_rid, idu, tex, geo_commune, creat_date, update_dat, lot, ogc_fid, geom
)
SELECT
    fe.code_dep || '0' || fe.code_com || fe.com_abs || fe.section AS geo_section,
    (SELECT annee FROM import) AS annee,
    'Object_' AS object_rid,
    fe.code_com || fe.com_abs || fe.section AS idu,
    LTRIM(fe.com_abs, '0') || fe.section AS tex,
    fe.code_dep || '0' || fe.code_com AS geo_commune,
    NOW()::date AS creat_date,
    NOW()::date AS update_dat,
    (SELECT lot FROM import) AS lot,
    ROW_NUMBER() OVER() + (SELECT MAX(ogc_fid) FROM qgis.geo_section) AS ogc_fid,
    fe.wkb_geometry AS geom
FROM import.feuille fe
WHERE
    fe.code_dep || fe.code_com IN (
        '67001', '67043', '67049', '67118', '67124', '67131',
        '67137', '67152', '67204', '67212', '67218', '67256',
        '67267', '67268', '67296', '67309', '67326', '67343',
        '67365', '67378', '67389', '67447', '67471', '67482',
        '67506', '67519', '67551'
    )
;  -- 1191

INSERT INTO qgis.geo_parcelle (
    geo_parcelle, annee, object_rid, idu, geo_section, geo_subdsect, supf, geo_indp, coar, tex, tex2,
    codm, creat_date, update_dat, inspireid, lot, ogc_fid, geom
)
SELECT
    pa.code_dep || '0' || pa.code_com || pa.com_abs || pa.section || pa.numero AS geo_parcelle,
    (SELECT annee FROM import) annee,
    'Objet_' AS object_rid,
    pa.code_com || pa.com_abs || pa.section || pa.numero AS idu,
    pa.code_dep || '0' || pa.code_com || pa.com_abs || pa.section AS geo_section,
    NULL AS geo_subdsect,
    pa.contenance::int AS supf,
    NULL AS geo_indp,
    fo.ccoarp AS coar,
    LTRIM(pa.numero, '0') AS tex,
    NULL AS tex2,
    NULL AS codm,
    NOW()::date AS creat_date,
    NOW()::date AS update_dat,
    'FR' || pa.code_dep || '0' || pa.code_com || pa.com_abs || pa.section || pa.numero AS inspireid,
    (SELECT lot FROM import) AS lot,
    ROW_NUMBER() OVER() + (SELECT MAX(ogc_fid) FROM qgis.geo_parcelle) AS ogc_fid,
    pa.wkb_geometry AS geom
FROM
    import.parcelle pa
    LEFT JOIN "Foncier_SIG".parcelle_67 fo ON pa.idu = fo.idpar
WHERE
    pa.code_dep || pa.code_com IN (
        '67001', '67043', '67049', '67118', '67124', '67131',
        '67137', '67152', '67204', '67212', '67218', '67256',
        '67267', '67268', '67296', '67309', '67326', '67343',
        '67365', '67378', '67389', '67447', '67471', '67482',
        '67506', '67519', '67551'
    )
;  -- 207571

INSERT INTO qgis.geo_label (
    ogc_fid, object_rid, fon, hei, tyu, cef, csp, di1, di2, di3, di4, tpa, hta, vta, atr,
    ogr_obj_lnk, ogr_obj_lnk_layer, ogr_atr_val, ogr_angle, ogr_font_size, x_label, y_label, geom
)
SELECT
    ROW_NUMBER() OVER() + (SELECT MAX(ogc_fid) FROM qgis.geo_label) AS ogc_fid,
    'Attribut_TEXT_id_Objet_' AS object_rid,
    'Times New Roman', 2, 1, 1, 0.2, 0, 1, 1, 0, 1, 1, 1, 'TEX_id', 'Objet_', 'PARCELLE_id',
    LTRIM(pa.numero, '0') AS ogc_atr_val,
    0, 4,
    -- la bbox de l'etiquette fait 1 m / caractere
    ST_X(ST_PointOnSurface(pa.wkb_geometry)) - (LENGTH(LTRIM(pa.numero, '0')) * 0.5) AS x_label,
    -- la bbox de l'etiquette fait 2.2 m
    ST_Y(ST_PointOnSurface(pa.wkb_geometry)) - 1.1 AS y_label,
    ST_SetSRID(
        ST_MakePoint(
            ST_X(ST_PointOnSurface(pa.wkb_geometry)) - (LENGTH(LTRIM(pa.numero, '0')) * 0.5),
            ST_Y(ST_PointOnSurface(pa.wkb_geometry)) - 1.1
        ),
        2154
    ) AS geom
FROM import.parcelle pa
WHERE
    pa.code_dep || pa.code_com IN (
        '67001', '67043', '67049', '67118', '67124', '67131',
        '67137', '67152', '67204', '67212', '67218', '67256',
        '67267', '67268', '67296', '67309', '67326', '67343',
        '67365', '67378', '67389', '67447', '67471', '67482',
        '67506', '67519', '67551'
    )
;  -- 207571

-- VERIFIER DOUBLONS SUR 67001 ACHENHEIM