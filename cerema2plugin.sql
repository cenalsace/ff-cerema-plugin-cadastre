-- PARAMETRES
CREATE TEMPORARY TABLE import AS
    SELECT
        'import_cen'::text AS lot,
        2022::int AS annee
;

-- FANTOIR 67, 68, 57
-- https://www.collectivites-locales.gouv.fr/competences/la-mise-disposition-gratuite-du-fichier-des-voies-et-des-lieux-dits-fantoir
\COPY qgis.fanr(tmp) FROM '670.txt' DELIMITER ',' ENCODING 'UTF8';
\COPY qgis.fanr(tmp) FROM '680.txt' DELIMITER ',' ENCODING 'UTF8';
\COPY qgis.fanr(tmp) FROM '570.txt' DELIMITER ',' ENCODING 'UTF8';

-- parcelles
INSERT INTO qgis.parcelle (
    parcelle, annee, ccodep, ccodir, ccocom, ccopre, ccosec, dnupla, dcntpa, dnupro,
    comptecommunal, jdatat, dreflf, gpdl, cprsecr, ccosecr, dnuplar, dnupdl, pdl, gurbpa,
    dparpi, ccoarp, gparnf, gparbat, dnvoiri, dindic, ccovoi, ccoriv, ccocif, cconvo,
    dvoilib, ccocomm, ccoprem, ccosecm, dnuplam, lot
)
SELECT
    p.ccodep || p.ccodir || p.ccocom || COALESCE(p.ccopre, '000') || RIGHT('00' || p.ccosec, 2) || p.dnupla,
    (SELECT annee::text FROM import), p.ccodep, p.ccodir, p.ccocom, p.ccopre, p.ccosec, p.dnupla,
    p.dcntpa, p.dnupro, p.idprocpte, p.jdatat, p.dreflf, p.gpdl, p.cprsecr, p.ccosecr, p.dnuplar,
    p.dnupdl, p.pdlmp, p.gurbpa, p.dparpi, p.ccoarp, p.gparnf, p.gparbat, p.dnuvoi, p.dindic,
    p.ccovoi, p.ccoriv, p.ccocif, p.cconvo, p.dvoilib, p.ccocomm, p.ccoprem, p.ccosecm, p.dnuplam,
    (SELECT lot FROM import)
FROM ff2021.fftp_2021_pnb10_parcelle p
;

-- proprietaires
INSERT INTO qgis.proprietaire (
    proprietaire,
    annee, ccodep, ccodir, ccocom, dnupro, comptecommunal, dnulp, ccocif, dnuper, ccodro,
    ccodem, gdesip, gtoper, ccoqua, dnatpr, ccogrm, dsglpm, dforme, ddenom, gtyp3, dlign3,
    gtyp4, dlign4, gtyp5, dlign5, gtyp6, dlign6, ccopay, ccodep1a2, ccodira, ccocom_adr,
    ccovoi, ccoriv, dnvoiri, dindic, ccopos, dqualp, dnomlp, dprnlp, jdatnss, dldnss,
    dsiren, topja, datja, dformjur, dnomus, dprnus, lot
)
SELECT
    p.ccodep || '_' || p.idpk,
    (SELECT annee::text FROM import), p.ccodep, p.ccodir, p.ccocom, p.dnupro, p.idprocpte,
    p.dnulp, p.ccocif, p.dnuper, p.ccodro, p.ccodem, p.gdesip, p.gtoper, p.ccoqua, p.dnatpr,
    p.ccogrm, p.dsglpm, p.dforme, p.ddenom, p.gtyp3, p.dlign3, p.gtyp4, p.dlign4, p.gtyp5,
    p.dlign5, p.gtyp6, p.dlign6, p.ccopay, p.ccodep1a2, p.ccodira, p.ccocomadr, p.ccovoi,
    p.ccoriv, p.dnvoiri, p.dindic, p.ccopos, p.dqualp, p.dnomlp, p.dprnlp, p.jdatnss,
    p.dldnss, p.dsiren, p.topja, p.datja, p.dformjur, p.dnomus, p.dprnus,
    (SELECT lot FROM import)
FROM ff2021.fftp_2021_proprietaire_droit_non_ano p
WHERE
    EXISTS (
        SELECT 1
        FROM qgis.parcelle
        WHERE parcelle.comptecommunal = p.idprocpte
    )
    AND NOT EXISTS (
        SELECT 1
        FROM qgis.proprietaire pro
        WHERE p.ccodep || '_' || p.idpk::text = pro.proprietaire
    )
;

-- lots
INSERT INTO qgis.lots (
    lots, annee, ccodep, ccodir, ccocom, ccopre, ccosec, dnupla, parcelle, dnupdl, dnulot,
    cconlo, dcntlo, dnumql, ddenql, dfilot, datact, dnuprol, comptecommunal, dreflf, ccocif, lot
)
SELECT
    l.idlot, (SELECT annee::text FROM import), l.ccodep, l.ccodir, l.ccocom, l.ccopre, l.ccosec,
    l.dnupla, l.ccodep || l.ccodir || l.ccocom || COALESCE(l.ccopre, '000') || l.ccosec || l.dnupla,
    l.dnupdl, l.dnulot, l.cconlo, l.dcntlo, l.dnumql, l.ddenql, l.dfilot, l.datact,
    l.dnuprol, l.idprocpte, l.dreflf, l.ccocif, (SELECT lot FROM import)
FROM ff2021.fftp_2021_pdl30_lots l
WHERE
    EXISTS (
        SELECT 1
        FROM qgis.parcelle
        WHERE parcelle.comptecommunal = l.idprocpte
    )
;

-- /cadastre/scripts/plugin/2022/majic3_formatage_donnees.sql[964]
-- Traitement: commune
INSERT INTO qgis.commune
(
 commune, geo_commune, annee, ccodep, ccodir, ccocom, clerivili, libcom, typcom, ruract, carvoi, indpop, poprel, poppart, popfict, annul, dteannul, dtecreart, codvoi,
 typvoi, indldnbat, motclas, lot
)
SELECT
  REPLACE(SUBSTRING(tmp,1,6),' ', '0') AS commune,
  REPLACE(SUBSTRING(tmp,1,6),' ', '0') AS geo_commune,
  (SELECT annee::text FROM import),
  SUBSTRING(tmp,1,2) AS ccodep,
  SUBSTRING(tmp,3,1) AS ccodir,
  SUBSTRING(tmp,4,3) AS ccocom,
  SUBSTRING(tmp,11,1) AS clerivili,
  SUBSTRING(tmp,12,30) AS libcom,
  CASE WHEN trim(SUBSTRING(tmp,43,1))='' THEN NULL ELSE trim(SUBSTRING(tmp,43,1)) END AS typcom,
  SUBSTRING(tmp,46,1) AS ruract,
  SUBSTRING(tmp,49,1) AS carvoi,
  SUBSTRING(tmp,50,1) AS indpop,
  CASE WHEN trim(SUBSTRING(tmp,53,7))='' THEN NULL ELSE to_number(trim(SUBSTRING(tmp,53,7)),'0000000') END AS poprel,
  to_number(SUBSTRING(tmp,60,7),'9999999') AS poppart,
  to_number(SUBSTRING(tmp,67,7),'0000000') AS popfict,
  SUBSTRING(tmp,74,1) AS annul,
  SUBSTRING(tmp,75,7) AS dteannul,
  SUBSTRING(tmp,82,7) AS dtecreart,
  SUBSTRING(tmp,104,5) AS codvoi,
  SUBSTRING(tmp,109,1) AS typvoi,
  SUBSTRING(tmp,110,1) AS indldnbat,
  SUBSTRING(tmp,113,8) AS motclas,
  (SELECT lot FROM import) as lot
FROM qgis.fanr WHERE SUBSTRING(tmp,4,3)  != ' ' AND trim(SUBSTRING(tmp,7,4))=''
;
-- /cadastre/scripts/plugin/2022/majic3_formatage_donnees.sql[996]
-- Traitement: voie
INSERT INTO qgis.voie
(
 voie, annee, ccodep, ccodir, ccocom, natvoiriv, ccoriv, clerivili, natvoi, libvoi, typcom, ruract, carvoi, indpop, poprel, poppart, popfict, annul, dteannul,
 dtecreart, codvoi, typvoi, indldnbat, motclas,
 commune, lot
)
SELECT
  REPLACE(SUBSTRING(tmp,1,6)||SUBSTRING(tmp,104,5)||SUBSTRING(tmp,7,4),' ', '0') AS voie,
  (SELECT annee::text FROM import),
  SUBSTRING(tmp,1,2) AS ccodep,
  SUBSTRING(tmp,3,1) AS ccodir,
  SUBSTRING(tmp,4,3) AS ccocom,
  CASE WHEN trim(SUBSTRING(tmp,7,1))='' THEN NULL ELSE trim(SUBSTRING(tmp,7,1)) END AS natvoiriv,
  SUBSTRING(tmp,7,4) AS ccoriv,
  SUBSTRING(tmp,11,1) AS clerivili,
  TRIM(SUBSTRING(tmp,12,4)) AS natvoi,
  SUBSTRING(tmp,16,26) AS libvoi,
  CASE WHEN trim(SUBSTRING(tmp,43,1))='' THEN NULL ELSE trim(SUBSTRING(tmp,43,1)) END AS typcom,
  SUBSTRING(tmp,46,1) AS ruract,
  CASE WHEN trim(SUBSTRING(tmp,49,1))='' THEN NULL ELSE trim(SUBSTRING(tmp,49,1)) END AS carvoi,
  SUBSTRING(tmp,50,1) AS indpop,
  SUBSTRING(tmp,53,7) AS poprel,
  to_number(SUBSTRING(tmp,60,7),'0000000') AS poppart,
  to_number(SUBSTRING(tmp,67,7),'0000000') AS popfict,
  CASE WHEN trim(SUBSTRING(tmp,74,1))='' THEN NULL ELSE trim(SUBSTRING(tmp,74,1)) END AS annul,
  SUBSTRING(tmp,75,7) AS dteannul,
  SUBSTRING(tmp,82,7) AS dtecreart,
  SUBSTRING(tmp,104,5) AS codvoi,
  CASE WHEN trim(SUBSTRING(tmp,109,1))='' THEN NULL ELSE trim(SUBSTRING(tmp,109,1)) END AS typvoi,
  CASE WHEN trim(SUBSTRING(tmp,110,1))='' THEN NULL ELSE trim(SUBSTRING(tmp,110,1)) END AS indldnbat,
  SUBSTRING(tmp,113,8) AS motclas,
  REPLACE(SUBSTRING(tmp,1,6),' ', '0') AS commune,
  (SELECT lot FROM import) as lot
FROM qgis.fanr WHERE trim(SUBSTRING(tmp,4,3))  != '' AND trim(SUBSTRING(tmp,7,4))  != ''
;

-- purge des doublons : voie
CREATE INDEX IF NOT EXISTS idxan_voie ON qgis.voie (annee);
-- /cadastre/scripts/plugin/2022/majic3_formatage_donnees.sql[1035]
-- INDEXES
CREATE INDEX IF NOT EXISTS idxan_suf ON qgis.suf (annee);
CREATE INDEX IF NOT EXISTS idxan_sufexoneration ON qgis.sufexoneration (annee);
CREATE INDEX IF NOT EXISTS idxan_suftaxation ON qgis.suftaxation (annee);
CREATE INDEX IF NOT EXISTS idxan_pev ON qgis.pev (annee);
CREATE INDEX IF NOT EXISTS idxan_pevexoneration_imposable ON qgis.pevexoneration_imposable (annee);
CREATE INDEX IF NOT EXISTS idxan_pevexoneration_imposee ON qgis.pevexoneration_imposee (annee);
CREATE INDEX IF NOT EXISTS idxan_pevtaxation ON qgis.pevtaxation (annee);
CREATE INDEX IF NOT EXISTS idxan_pevprincipale ON qgis.pevprincipale (annee);
CREATE INDEX IF NOT EXISTS idxan_pevprofessionnelle ON qgis.pevprofessionnelle (annee);
CREATE INDEX IF NOT EXISTS idxan_pevdependances ON qgis.pevdependances (annee);
CREATE INDEX IF NOT EXISTS idxan_pdl ON qgis.pdl (annee);
CREATE INDEX IF NOT EXISTS idxan_parcellecomposante ON qgis.parcellecomposante (annee);
CREATE INDEX IF NOT EXISTS idx_lots_tmp1 ON qgis.lots (annee, ccodep, ccodir, ccocom, dnuprol);
CREATE INDEX IF NOT EXISTS idxan_lotslocaux ON qgis.lotslocaux (annee);
CREATE INDEX IF NOT EXISTS idxan_commune ON qgis.commune (annee);
CREATE INDEX IF NOT EXISTS proprietaire_dnupro_idx ON qgis.proprietaire (dnupro);
CREATE INDEX IF NOT EXISTS proprietaire_ddenom_idx ON qgis.proprietaire (ddenom);
CREATE INDEX IF NOT EXISTS parcelle_dnupro_idx ON qgis.parcelle (dnupro);
CREATE INDEX IF NOT EXISTS suf_parcelle_idx ON qgis.suf (parcelle);
CREATE INDEX IF NOT EXISTS sufexoneration_suf_idx ON qgis.sufexoneration (suf);
CREATE INDEX IF NOT EXISTS idx_proprietaire_ccocom  ON qgis.proprietaire (ccocom);
CREATE INDEX IF NOT EXISTS idx_commune_ccocom  ON qgis.commune (ccocom);
CREATE INDEX IF NOT EXISTS idx_proprietaire_ccodro  ON qgis.proprietaire (ccodro);
CREATE INDEX IF NOT EXISTS idx_proprietaire_comptecommunal ON qgis.proprietaire (comptecommunal);
CREATE INDEX IF NOT EXISTS idx_local00_parcelle  ON qgis.local00 (parcelle);
CREATE INDEX IF NOT EXISTS idx_local00_voie  ON qgis.local00 (voie);
CREATE INDEX IF NOT EXISTS idx_local10_local00  ON qgis.local10 (local00);
CREATE INDEX IF NOT EXISTS idx_local10_comptecommunal  ON qgis.local10 (comptecommunal);
CREATE INDEX IF NOT EXISTS idx_pevexoneration_imposable_pev ON qgis.pevexoneration_imposable (pev);
CREATE INDEX IF NOT EXISTS idx_pevexoneration_imposee_pev ON qgis.pevexoneration_imposee (pev);
CREATE INDEX IF NOT EXISTS idx_pevtaxation_pev ON qgis.pevtaxation (pev);
CREATE INDEX IF NOT EXISTS idx_parcelle_voie ON qgis.parcelle (voie);
CREATE INDEX IF NOT EXISTS idx_parcelle_comptecommunal ON qgis.parcelle (comptecommunal);

-- effacer les donnees fantr temporaires
TRUNCATE TABLE qgis.fanr
;

-- /cadastre/scripts/plugin/edigeo_create_table_parcelle_info_majic.sql
BEGIN;

-- Création la table parcelle_info ( EDIGEO + MAJIC )
DROP TABLE IF EXISTS qgis.parcelle_info;

CREATE TABLE qgis.parcelle_info
(
  ogc_fid integer,
  geo_parcelle text,
  idu text,
  tex text,
  geo_section text,
  nomcommune text,
  codecommune text,
  surface_geo bigint,
  contenance bigint,
  parcelle_batie text,
  adresse text,
  urbain text,
  code text,
  comptecommunal text,
  voie text,
  proprietaire text,
  proprietaire_info text,
  lot text,
  geom GEOMETRY('MULTIPOLYGON', 2154)
);

CREATE INDEX aa ON qgis.parcelle (parcelle);
CREATE INDEX bb ON qgis.parcelle (comptecommunal);
CREATE INDEX cc ON qgis.parcelle (ccocom);
CREATE INDEX dd ON qgis.parcelle (ccodep);
CREATE INDEX ee ON qgis.parcelle (voie);
CREATE INDEX ff ON qgis.proprietaire (comptecommunal);
CREATE INDEX gg ON qgis.geo_parcelle (geo_parcelle);
CREATE INDEX hh ON qgis.commune (ccocom);
CREATE INDEX ii ON qgis.commune (ccodep);
CREATE INDEX jj ON qgis.voie (voie);

INSERT INTO qgis.parcelle_info
SELECT gp.ogc_fid AS ogc_fid, gp.geo_parcelle, gp.idu AS idu, gp.tex AS tex, gp.geo_section AS geo_section,
c.libcom AS nomcommune, c.ccocom AS codecommune, Cast(ST_Area(gp.geom) AS bigint) AS surface_geo, p.dcntpa AS contenance,
CASE
    WHEN coalesce(p.gparbat, '0') = '1' THEN 'Oui'
    ELSE 'Non'
END AS parcelle_batie,
CASE
        WHEN v.libvoi IS NOT NULL THEN trim(ltrim(p.dnvoiri, '0') || ' ' || trim(v.natvoi) || ' ' || v.libvoi)
        ELSE ltrim(p.cconvo, '0') || p.dvoilib
END AS adresse,
CASE
        WHEN p.gurbpa = 'U' THEN 'Oui'
        ELSE 'Non'
END  AS urbain,
ccosec || dnupla AS code,
p.comptecommunal AS comptecommunal, p.voie AS voie,

string_agg(
    trim(
        pr.dnuper || ' - ' ||
        trim(coalesce(pr.dqualp, '')) || ' ' ||
        trim(coalesce(pr.ddenom, '')) || ' - ' ||
        ccodro_lib
    ),
    '|'
) AS proprietaire,

string_agg(
    trim(
        pr.dnuper || ' - ' ||
        ltrim(trim(coalesce(pr.dlign4, '')), '0') ||
        trim(coalesce(pr.dlign5, '')) || ' ' ||
        trim(coalesce(pr.dlign6, '')) ||
        trim(
            CASE
                WHEN pr.jdatnss IS NOT NULL
                THEN ' - Né(e) le ' || pr.jdatnss || ' à ' || coalesce(pr.dldnss, '')
                ELSE ''
            END
        )
    ),
    '|'
) AS info_proprietaire,

gp.lot AS lot,
gp.geom AS geom
FROM qgis.geo_parcelle gp
LEFT OUTER JOIN qgis.parcelle p ON gp.geo_parcelle = p.parcelle
LEFT OUTER JOIN qgis.proprietaire pr ON p.comptecommunal = pr.comptecommunal
LEFT OUTER JOIN qgis.ccodro ON ccodro.ccodro = pr.ccodro
LEFT OUTER JOIN qgis.commune c ON p.ccocom = c.ccocom AND c.ccodep = p.ccodep
LEFT OUTER JOIN qgis.voie v ON v.voie = p.voie
GROUP BY gp.geo_parcelle, gp.ogc_fid, gp.idu, gp.tex, gp.geo_section, gp.lot,
c.libcom, c.ccocom, gp.geom, p.dcntpa, v.libvoi, p.dnvoiri, v.natvoi,
p.comptecommunal, p.cconvo, p.voie, p.dvoilib, p.gurbpa, p.gparbat,
ccosec, dnupla
;

DROP INDEX qgis.aa;
DROP INDEX qgis.bb;
DROP INDEX qgis.cc;
DROP INDEX qgis.dd;
DROP INDEX qgis.ee;
DROP INDEX qgis.ff;
DROP INDEX qgis.gg;
DROP INDEX qgis.hh;
DROP INDEX qgis.ii;
DROP INDEX qgis.jj;

ALTER TABLE qgis.parcelle_info ADD CONSTRAINT parcelle_info_pk PRIMARY KEY (ogc_fid);
CREATE INDEX parcelle_info_geom_idx ON qgis.parcelle_info USING gist (geom);
CREATE INDEX parcelle_info_geo_section_idx ON qgis.parcelle_info (geo_section);
CREATE INDEX parcelle_info_comptecommunal_idx ON qgis.parcelle_info (comptecommunal);
CREATE INDEX parcelle_info_codecommune_idx ON qgis.parcelle_info (codecommune);
CREATE INDEX parcelle_info_geo_parcelle_idx ON qgis.parcelle_info (geo_parcelle);

COMMENT ON TABLE qgis.parcelle_info IS 'Table de parcelles consolidées, proposant les géométries et les informations MAJIC principales, dont les propriétaires';

COMMENT ON COLUMN qgis.parcelle_info.ogc_fid IS 'Identifiant unique (base de données)';
COMMENT ON COLUMN qgis.parcelle_info.geo_parcelle IS 'Identifiant de la parcelle : année + département + direction + idu';
COMMENT ON COLUMN qgis.parcelle_info.idu IS 'Identifiant de la parcelle (unique par département et direction seulement)';
COMMENT ON COLUMN qgis.parcelle_info.tex IS 'Etiquette (maximum 3 caractères, ex: 1 ou 24)';
COMMENT ON COLUMN qgis.parcelle_info.geo_section IS 'Code de la section (lien vers table geo_section.geo_section)';
COMMENT ON COLUMN qgis.parcelle_info.nomcommune IS 'Nom de la commune';
COMMENT ON COLUMN qgis.parcelle_info.codecommune IS 'Code de la commune à 3 chiffres, ex: 021';
COMMENT ON COLUMN qgis.parcelle_info.surface_geo IS 'Surface de la parcelle, calculée spatialement';
COMMENT ON COLUMN qgis.parcelle_info.contenance IS 'Contenance de la parcelle (information MAJIC)';
COMMENT ON COLUMN qgis.parcelle_info.parcelle_batie IS 'Indique si la parcelle est bâtie ou non (issu du champ parcelle.gparbat)';
COMMENT ON COLUMN qgis.parcelle_info.adresse IS 'Adresse de la parcelle';
COMMENT ON COLUMN qgis.parcelle_info.urbain IS 'Déclare si la parcelle est urbaine ou non';
COMMENT ON COLUMN qgis.parcelle_info.code IS 'Code de la parcelle (6 caractères, ex: AB0001)';
COMMENT ON COLUMN qgis.parcelle_info.comptecommunal IS 'Compte communal du propriétaire';
COMMENT ON COLUMN qgis.parcelle_info.voie IS 'Code de la voie (lien avec voie)';
COMMENT ON COLUMN qgis.parcelle_info.proprietaire IS 'Information sur les propriétaires: code DNUPER, nom, et type. Les informations sont séparées par | entre propriétaires.';
COMMENT ON COLUMN qgis.parcelle_info.proprietaire_info IS 'Informations détaillées sur les propriétaires bis: code DNUPER, adresse, date et lieu de naissance. Les informations sont séparées par | entre propriétaires.';
COMMENT ON COLUMN qgis.parcelle_info.lot IS 'Lot utilisé pendant l''import';

COMMIT;

/*
-- A executer puis a jouer
SELECT 'UPDATE TABLE ' || table_schema || '.' || table_name || ' SET ' || column_name || ' = (SELECT lot FROM import);'
FROM information_schema.columns
WHERE
    (table_name LIKE 'geo_%' OR table_name = 'parcelle_info')
    AND column_name = 'lot'
;
*/
