@ECHO OFF
SET PGPASSWORD=###
SET PGHOST=###.###.###.###
SET PGPORT=####
SET PGUSER=###
SET PGBASE=###
SET PCIEXP67="https://wxs.ign.fr/vxlh30ais2rjyt2nb4ivupn2/telechargement/prepackage/PARCELLAIRE-EXPRESS_PACK-FXX_et_DOM_2022-04-01$PARCELLAIRE_EXPRESS_1-0__SHP_LAMB93_D067_2022-04-01/file/PARCELLAIRE_EXPRESS_1-0__SHP_LAMB93_D067_2022-04-01.7z"
SET SEVENZ="C:\Program Files\7-Zip\7z"

@REM Create schema to import PCI Express data
psql -U %PGUSER% -h %PGHOST% -p %PGPORT% -d %PGBASE% -q -c "CREATE SCHEMA IF NOT EXISTS import;"

@REM Download PCI Express for 67
curl -k %PCIEXP67% --output PCIEXP67.7z

@REM Extract and load PCI Express data into schema import
FOR %%i IN (COMMUNE,FEUILLE,PARCELLE) DO (
    %SEVENZ% e *.7z -r %%i.* > nul
    ogr2ogr -f "PostgreSQL" PG:"host='%PGHOST%' user='%PGUSER%' dbname='%PGBASE%'" %%i.shp -nlt MULTIPOLYGON -nln "import.%%i"
    DEL %%i.*
)

@REM Execute the SQL script for EMS municipalities then drop schema import
psql -U %PGUSER% -h %PGHOST% -p %PGPORT% -d %PGBASE% -q -f ajout_eurometropole.sql
psql -U %PGUSER% -h %PGHOST% -p %PGPORT% -d %PGBASE% -q -c "DROP SCHEMA IF EXISTS import CASCADE;"
